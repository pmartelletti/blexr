## Project Setup
- Clone the project
- cd to the project directory and run `composer install` to install all dependencies.
- Copy the file `.env.example` into `.env` and replace with your DB and SMTP MAIL configuration.
- Run the setup script to generate all MySQL tables: `php public/setup.php`. This will automatically generate all tables needed.
- Optionally, seed the system with some data by running `php public/load-sample-data.php`

## Using the PHP API
The system consists of an API written in PHP, and a (incomplete) UI written in vue.js. In order to start using the API, we can make use of the built in server. To start it, just run: 
    `php -S localhost:8088 -t public/`
    
This should start the API on port 8088 (that's the one the UI is expecting).

### Obtaining an access token
In order to use the API, you need to pass an access token to each HTTP request. That token is expected in the `Authorization` header, and the value of it should be in the format `Bearer your-token-here`.
You can obtain a API token by doing a POST request to `http://localhost:8088/oauth/token` and sending in the body (as JSON) the username and password of the user. This will return an access token, which you can use to make all the other authenticated requests.

## Using the vue.js UI
The UI for the API is still a work in progress, but some screens are already available. In order to access it, you'd need to install all npm dependencies and then start the development server. For that, you'd just need to run:

```
cd ui
npm install
npm run serve
```

This will start the nodejs development server in you localhost, port 8080.

    
    
  