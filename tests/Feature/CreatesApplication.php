<?php
namespace Tests\Feature;

use App\Framework\Application;

trait CreatesApplication
{
    protected function createApplication()
    {
        // this probably can change when we use .env to define the config
        return new Application();
    }
}
