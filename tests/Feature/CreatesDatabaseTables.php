<?php
namespace Tests\Feature;


use App\Database\AccessTokenSchema;
use App\Database\LicenseSchema;
use App\Database\LocationSchema;
use App\Database\RemoteWorkingRequestSchema;
use App\Database\UserLicenseSchema;
use App\Database\UserSchema;
use App\Framework\Application;

trait CreatesDatabaseTables
{
    public function createDatabaseTables(Application $application)
    {
        (new UserSchema($application))->create();
        (new LocationSchema($application))->create();
        (new RemoteWorkingRequestSchema($application))->create();
        (new LicenseSchema($application))->create();
        (new UserLicenseSchema($application))->create();
        (new AccessTokenSchema($application))->create();
    }
}
