<?php
namespace Tests\Feature\UserApi;

use App\Models\Location;
use App\Models\RemoteWorkingRequest;
use App\Models\User;
use Carbon\Carbon;
use Tests\Feature\ApiTest;

class UserRequestsTest extends ApiTest
{
    protected $user;

    public function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $this->user = $this->createUser();;
        $this->withHeader('Authorization', 'Bearer ' . $this->user->issueToken());
    }

    /** @test */
    public function user_can_see_a_list_of_their_work_from_home_request()
    {
        $this->createRequest($this->user);

        $response = $this->getJson('/api/me/remote-working-requests');
        $response->assertOk()
            ->assertJsonCount(1)
            ->assertJsonFragment([
                'employee_is_sick' => false,
                'location_id' => "1"
            ]);
    }

    /** @test */
    public function user_can_cancel_a_work_from_home_request()
    {
        $request = $this->createRequest($this->user);

        $this
            ->putJson('/api/me/remote-working-requests/' . $request->id, [
                'status' => 'cancelled'
            ])
            ->assertOk()
            ->assertJsonFragment([
                'status' => 'cancelled'
            ]);

        $this->assertEquals('cancelled', $request->fresh()->status);
    }

    /** @test */
    public function user_can_request_to_work_remotly()
    {
        $response = $this->postJson('/api/me/remote-working-requests', [
            'employee_is_sick' => false,
            'location_id' => Location::create(['name' => 'Malta Office'])->id,
            'requested_date' => Carbon::today()->addWeek()->format('d-m-Y')
        ]);

        $response->assertOk()
            ->assertJsonFragment([
                'employee_is_sick' => false,
                'location_id' => 1, // an other office or his home
                'requested_date' => Carbon::today()->addWeek()->format('d-m-Y')
            ])
        ;
        $this->assertEquals(1, $this->user->fresh()->remoteWorkingRequests()->count());
    }

    /** @test */
    public function next_day_requests_cannot_be_done_after_4pm()
    {
        // fake current time
        Carbon::setTestNow(Carbon::today()->setTime(17, 0));

        $response = $this->postJson('/api/me/remote-working-requests', [
            'employee_is_sick' => false,
            'location_id' => Location::create(['name' => 'Malta Office'])->id,
            'requested_date' => Carbon::tomorrow()->format('d-m-Y')
        ]);

        $response
            ->assertStatus(400)
            ->assertJsonFragment([
                'message' => 'Requests should be done at least 8 hours before the end of previous requested day.'
            ])
        ;
    }

    /** @test */
    public function user_can_make_a_same_day_request_to_work_from_home_when_sick()
    {
        // fake current time
        Carbon::setTestNow(Carbon::today()->setTime(7, 59));

        $response = $this->postJson('/api/me/remote-working-requests', [
            'employee_is_sick' => true,
            'location_id' => Location::create(['name' => "Employee's Home"])->id,
            'requested_date' => Carbon::today()->format('d-m-Y')
        ]);

        $response
            ->assertOk()
            ->assertJsonFragment([
                'employee_is_sick' => true,
                'location_id' => 1, // an other office or his home
                'requested_date' => Carbon::today()->format('d-m-Y')
            ])
        ;
    }

    /** @test */
    public function same_day_requests_in_case_of_sicknes_cant_be_done_after_8am()
    {
        // fake current time
        Carbon::setTestNow(Carbon::today()->setTime(8, 01));

        $response = $this->postJson('/api/me/remote-working-requests', [
            'employee_is_sick' => true,
            'location_id' => Location::create(['name' => "Employee's Home"])->id,
            'requested_date' => Carbon::today()->format('d-m-Y')
        ]);

        $response
            ->assertStatus(400)
            ->assertJsonFragment([
                'message' => 'Same day requests in case of sickens should be made by 8 am.'
            ])
        ;
    }

    protected function createRequest(User $user, $data = [])
    {
        return RemoteWorkingRequest::create(array_replace([
            'user_id' => $user->id,
            'employee_is_sick' => false,
            'location_id' => Location::create(['name' => 'Malta Office'])->id,
            'requested_date' => Carbon::today()->addWeek()->format('d/m/Y')
        ], $data));
    }

    protected function createUser($data = [])
    {
        return User::create(array_replace([
            'first_name' => 'Pablo',
            'last_name' => 'Martelletti',
            'password' => '123456',
            'email' => 'developer@example.com',
            'job_title' => 'PHP Developer',
            'is_admin' => false
        ], $data));
    }
}
