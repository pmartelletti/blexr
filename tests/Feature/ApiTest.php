<?php
namespace Tests\Feature;

use PHPUnit\Framework\TestCase;

abstract class ApiTest extends TestCase
{
    use MakesHttpRequests, CreatesApplication, CreatesDatabaseTables;

    protected $app;

    protected function setUp(): void
    {
        parent::setUp();
        $this->app = $this->createApplication();
        $this->createDatabaseTables($this->app);
    }
}
