<?php
namespace Tests\Unit;

use Carbon\Carbon;
use App\Framework\Exception\InvalidRemoteWorkingRequestException;
use App\Models\RemoteWorkingRequest;
use PHPUnit\Framework\TestCase;

class RemoteWorkingRequestTest extends TestCase
{
    /** @test */
    public function it_should_not_allow_requests_after_deadline()
    {
        $this->expectException(InvalidRemoteWorkingRequestException::class);
        $this->validateScenario(new Carbon('2019-04-30 16:30'));
    }

    /** @test */
    public function it_should_allow_requests_before_deadline()
    {
        $validation = $this->validateScenario(new Carbon('2019-04-30 15:30'));
        $this->assertTrue($validation);
    }

    /** @test */
    public function it_should_allow_same_day_requests_in_case_of_sickness_before_8am()
    {
        $validation = $this->validateScenario(
            new Carbon('2019-05-1 7:30'),
            ['employee_is_sick' => true]
        );
        $this->assertTrue($validation);
    }

    /** @test */
    public function it_should_not_allow_same_day_requests_in_case_of_sickness_after_8am()
    {
        $this->expectException(InvalidRemoteWorkingRequestException::class);
        $this->validateScenario(
            new Carbon('2019-05-1 8:30'),
            ['employee_is_sick' => true]
        );
    }

    /** @test */
    public function it_should_not_allow_requests_in_the_past()
    {
        $this->expectException(InvalidRemoteWorkingRequestException::class);
        $this->validateScenario(
            new Carbon('2019-05-2 0:00'),
            ['employee_is_sick' => true]
        );
    }

    private function validateScenario(Carbon $now, $overrideData = [])
    {
        \Carbon\Carbon::setTestNow($now);
        $request = collect([
            'requested_date' => '2019-05-01',
            'employee_is_sick' => false,
            'location_id' => 1
        ])->merge($overrideData);

        return RemoteWorkingRequest::validate($request);
    }
}
