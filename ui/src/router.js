import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Locations from './views/Locations.vue'
import Licenses from './views/Licenses.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/admin/locations',
      name: 'Locations',
      component: Locations
    },
      {
          path: '/admin/licenses',
          name: 'Licenses',
          component: Licenses
      }
  ]
})
