import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import Menu from './components/Menu.vue'


Vue.config.productionTip = false
Vue.use(Vuetify)
Vue.component('v-menu', Menu)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
