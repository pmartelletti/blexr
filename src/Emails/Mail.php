<?php

namespace App\Emails;


interface Mail
{
    public function subject() : string;

    public function recipient() : string;

    public function body() : string;
}
