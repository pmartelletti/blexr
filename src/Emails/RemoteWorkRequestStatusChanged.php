<?php

namespace App\Emails;


use App\Models\RemoteWorkingRequest;
use App\Models\User;

class RemoteWorkRequestStatusChanged implements Mail
{
    /**
     * @var User
     */
    private $recipient;
    /**
     * @var RemoteWorkingRequest
     */
    private $request;

    public function __construct(User $recipient, RemoteWorkingRequest $request)
    {
        $this->recipient = $recipient;
        $this->request = $request;
    }

    public function subject(): string
    {
        return sprintf('Your remote work request has been %s', $this->request->status);
    }

    public function recipient(): string
    {
        return $this->recipient->email;
    }

    public function body(): string
    {
//        dd($this->request->requested_date);
        $body = <<<EOF
    <p>Dear {$this->recipient->first_name},</p>
    <p>This email is to let you know that your remote work request for the 
{$this->request->requested_date} changed its status to <strong>{$this->request->status}</strong></p>
    <p>If you have any further questions regarding this request, please contact HR department to clarify any issues you may have.</p>
    <p>Regards,</p>
EOF;
        return $body;
    }
}