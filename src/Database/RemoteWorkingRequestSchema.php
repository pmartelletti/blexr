<?php

namespace App\Database;


class RemoteWorkingRequestSchema extends Schema
{
    public function create()
    {
        $this->connection()->schema()->create('remote_working_requests', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->boolean('employee_is_sick');
            $table->integer('location_id')->unsigned();
            $table->date('requested_date');
            $table->enum('status', ['pending', 'cancelled', 'accepted', 'rejected'])->default('pending');
            $table->timestamps();
        });
    }

    public function drop()
    {
        $this->connection()->schema()->drop('remote_working_requests');
    }
}