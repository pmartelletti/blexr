<?php

namespace App\Database;


class LicenseSchema extends Schema
{
    public function create()
    {
        $this->connection()->schema()->create('licenses', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
    }

    public function drop()
    {
        $this->connection()->schema()->drop('licenses');
    }
}
