<?php

namespace App\Database;

use App\Framework\Application;

abstract class Schema
{
    /**
     * @var Application
     */
    private $application;

    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    protected function connection()
    {
        return $this->application->get('database');
    }

    public abstract function create();

    public abstract function drop();
}
