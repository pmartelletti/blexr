<?php

namespace App\Database;


class UserLicenseSchema extends Schema
{
    public function create()
    {
        $this->connection()->schema()->create('license_user', function ($table) {
            $table->integer('user_id');
            $table->integer('license_id');
            // TODO: setup FK
        });
    }

    public function drop()
    {
        $this->connection()->schema()->drop('license_user');
    }
}
