<?php

namespace App\Database;

class LocationSchema extends Schema
{
    public function create()
    {
        $this->connection()->schema()->create('locations', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
    }

    public function drop()
    {
        $this->connection()->schema()->drop('locations');
    }

}
