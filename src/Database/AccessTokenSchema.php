<?php
namespace App\Database;

class AccessTokenSchema extends Schema
{
    public function create()
    {
        $this->connection()->schema()->create('access_tokens', function ($table) {
            $table->string('id', 100)->primary();
            $table->integer('user_id')->index()->nullable();
            $table->boolean('revoked');
            $table->timestamps();
        });
    }

    public function drop()
    {
        $this->connection()->schema()->drop('access_tokens');
    }

}
