<?php
namespace App\Database;

class UserSchema extends Schema
{
    public function create()
    {
        $this->connection()->schema()->create('users', function ($table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('job_title');
            $table->boolean('is_admin')->default(false);
            $table->timestamps();
        });
    }

    public function drop()
    {
        $this->connection()->schema()->drop('users');
    }

}
