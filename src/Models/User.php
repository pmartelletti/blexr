<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $guarded = [];
    protected $casts = [
        'is_admin' => 'boolean'
    ];

    protected $hidden = [
        'password'
    ];

    public function licenses()
    {
        return $this->belongsToMany(License::class);
    }

    public function remoteWorkingRequests()
    {
        return $this->hasMany(RemoteWorkingRequest::class);
    }

    public function accessTokens()
    {
        return $this->hasMany(AccessToken::class);
    }

    public function issueToken() : string
    {
        $token = bin2hex(random_bytes(40));
        $this->accessTokens()->create([
            'id' => $token,
            'revoked' => false,
        ]);
        return $token;
    }

    public function isAdmin() : bool
    {
        return $this->is_admin;
    }
}
