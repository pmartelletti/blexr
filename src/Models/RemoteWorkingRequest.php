<?php

namespace App\Models;


use App\Framework\Exception\InvalidRemoteWorkingRequestException;
use App\Framework\Exception\ValidationException;
use App\Framework\Http\Request;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class RemoteWorkingRequest extends Model
{
    const REJECTED = 'rejected';
    const ACCEPTED = 'accepted';
    const PENDING = 'pending';
    const CANCELLED = 'cancelled';

    protected $guarded = [];
    protected $casts = [
        'employee_is_sick' => 'boolean',
    ];

    public function changeStatus($status)
    {
        // I'm assuming that, once a request is rejected, it cannot be changed
        if ($this->status === self::REJECTED) {
            throw new ValidationException("Can't change request status after it was rejected");
        }
        $this->status = $status;
        $this->save();
    }

    public static function validate(Collection $request)
    {
        $requestedDate = new Carbon($request->get('requested_date'));
        if (Carbon::today()->greaterThan($requestedDate)) {
            throw new InvalidRemoteWorkingRequestException('Requested date cannot be in the past');
        }
        // if the user is sick, request can be done by same day @ 8 am
        if ($request->get('employee_is_sick')) {
            if (Carbon::now()->greaterThan($requestedDate->setTime(8,0))) {
                throw new InvalidRemoteWorkingRequestException('Same day requests in case of sickens should be made by 8 am.');
            }
            return true;
        }
        // if it's a normal request, we check that it's done at least 8 hours before the end of previous day
        if (Carbon::now()->greaterThan($requestedDate->subHours(8))) {
            throw new InvalidRemoteWorkingRequestException('Requests should be done at least 8 hours before the end of previous requested day.');
        }
        // all is fine, no exception is thrown
        return true;
    }

    public function scopeFilter($query, Request $request)
    {
        // filter by status
        if (null !== $status = $request->get('status')) {
            return $query->where('status', $status);
        }

        if (null !== $user = $request->get('user')) {
            return $query->where('user_id', $user);
        }
    }
}
