<?php
namespace App\Framework\Http;

use App\Framework\ServiceContainer;
use App\Models\AccessToken;
use App\Models\User;

/**
 * Class Request. Heavily inspired by Symfony's and Laravel request, wrapping the PHP Global variables
 * into a OOP class, for easily handling of all the SERVER attributes of the request. This approach
 * also makes it much more easier to test any given request using PHPUnit.
 */
class Request
{
    private $query;
    private $request;
    private $server;
    private $content;
    private $container = null;

    public function __construct($query, $request, $server, $content = null)
    {
        $this->query = collect($query);
        $this->request = collect($request);
        $this->server = collect($server);
        $this->content = $content;
    }

    public function setContainer(ServiceContainer $container)
    {
        $this->container = $container;
    }

    /**
     * @return ServiceContainer | null
     */
    public function getContainer()
    {
        return $this->container;
    }


    /**
     * Gets a "parameter" value.
     *
     * Order of precedence: GET, PATH, POST
     *
     * @param $key
     * @param mixed $default The default value if the parameter key does not exist
     *
     * @return mixed|null
     */
    public function get($key, $default = null)
    {
        if (null !== $result = $this->query->get($key)) {
            return $result;
        }

        if (null !== $result = $this->request->get($key)) {
            return $result;
        }

        if (null !== $result = $this->getContent()->get($key)) {
            return $result;
        }

        return $default;
    }

    public function has($key)
    {
        return $this->getData()->has($key);
    }

    /**
     * Creates a new request based on PHP's globals ($_REQUEST, $_SERVER)
     *
     * @return Request
     */
    public static function createFromGlobals()
    {
        return new static($_GET, $_POST, $_SERVER);
    }

    /**
     * Create manual request - mainly used for testing purposes.
     *
     * @param $uri
     * @param string $method
     * @param array $parameters
     * @param array $server
     * @param null $content
     *
     * @return Request
     */
    public static function create($uri, $method = 'GET', $parameters = array(), $server = array(), $content = null)
    {
        $server['REQUEST_METHOD'] = strtoupper($method);
        switch (strtoupper($method)) {
            case 'POST':
            case 'PUT':
            case 'DELETE':
                if (!isset($server['CONTENT_TYPE'])) {
                    $server['CONTENT_TYPE'] = 'application/x-www-form-urlencoded';
                }
            // no break
            case 'PATCH':
                $request = $parameters;
                $query = array();
                break;
            default:
                $request = array();
                $query = $parameters;
                break;
        }

        $components = parse_url($uri);
        $queryString = '';
        if (isset($components['query'])) {
            parse_str(html_entity_decode($components['query']), $qs);
            if ($query) {
                $query = array_replace($qs, $query);
                $queryString = http_build_query($query, '', '&');
            } else {
                $query = $qs;
                $queryString = $components['query'];
            }
        } elseif ($query) {
            $queryString = http_build_query($query, '', '&');
        }
        $server['REQUEST_URI'] = $components['path'].('' !== $queryString ? '?'.$queryString : '');
        $server['QUERY_STRING'] = $queryString;

        return new self($query, $request, $server, $content);
    }

    public function getContent()
    {
        if (null === $this->content) {
            $this->content = file_get_contents('php://input');
        }

        // for now we only support content encode in json
        switch ($this->getContentType()) {
            case "application/json":
                return collect(json_decode($this->content, true));
            default:
                return $this->content;
        }
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->server->get('REQUEST_METHOD', 'GET');
    }

    /**
     * @return array
     */
    public function getContentAsArray()
    {
        return $this->getContent()->toArray();
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->server->get('REQUEST_URI', '/');
    }

    /**
     * Returns the content type of the request. If no content type is specified
     * on the request, default is JSON as this is the most used in WEB API
     *
     * @return mixed
     */
    public function getContentType()
    {
        $type = explode(';', $this->server->get('CONTENT_TYPE', 'application/json'), 2);

        return $type[0];
    }

    /**
     * Returns the logged in user for the current request
     *
     * @return User | null
     */
    public function user()
    {
        if (!$this->server->has('HTTP_AUTHORIZATION')) return null;
        list($type, $accessToken) = explode(' ', $this->server->get('HTTP_AUTHORIZATION'));
        // the authorization header is here, find the token it belongs to
        $token = AccessToken::find($accessToken);
        if (!$token) return null;
        return $token->user;
    }
}
