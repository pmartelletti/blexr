<?php

namespace App\Framework\Http;


class JsonResponse extends Response
{
    public function __construct($content = '', $status = 200, array $headers = array())
    {
        parent::__construct($content, $status, array_merge($headers, ['content-type' => 'application/json']));
    }
}
