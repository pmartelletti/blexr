<?php

namespace App\Framework\Exception;


use Throwable;

class Http401Unauthorized extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('Unauthorized', $code, $previous);
    }
}