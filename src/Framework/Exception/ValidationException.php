<?php

namespace App\Framework\Exception;


class ValidationException extends \Exception
{
    public function toJson()
    {
        return json_encode([
            'code' => $this->getCode(),
            'message' => $this->getMessage()
        ]);
    }
}