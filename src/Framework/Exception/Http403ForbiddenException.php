<?php

namespace App\Framework\Exception;

use Throwable;

class Http403ForbiddenException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('Forbidden', $code, $previous);
    }
}
