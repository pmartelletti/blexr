<?php
namespace App\Framework\Router;

use App\Framework\Exception\Http404NotFoundException;
use App\Framework\Http\Request;
use App\Framework\Http\Response;
use AltoRouter;

class ControllerResolver
{
    /** @var AltoRouter */
    private $router;

    public function __construct(AltoRouter $router)
    {
        $this->router = $router;
    }

    public function createResponse(Request $request): Response
    {
        if ($request->getMethod() === 'OPTIONS') {
            return new Response();
        }
        $match = $this->router->match($request->getUri(), $request->getMethod());
        list($controller, $action) = $this->getControllerAction($match);
        // call the method in the controller that match the current request
        return call_user_func_array(
            [new $controller($request), $action],
            $match['params']
        );
    }

    public function getControllerAction($match)
    {
        if (is_array($match) && array_key_exists('target', $match)) {
            $target = $match['target'];
            $parts = explode('#', $target);
            return [
                sprintf('App\\Controllers\\%s', $parts[0]),
                $parts[1]
            ];
        }
        // could not match the request, thrown an exception
        throw new Http404NotFoundException();
    }
}
