<?php
namespace App\Framework\Security;

use App\Framework\Exception\ValidationException;
use DateTimeInterface;

class Validator
{
    protected $parameterName;
    protected $rules;
    protected $data;

    public function __construct($parameterName, $data, $rules)
    {
        $this->parameterName = $parameterName;
        $this->data = $data;
        $this->rules = collect(explode("|", $rules));
    }

    public function validate()
    {
        if ($this->rules->contains('sometimes') && $this->data === null) {
            // the check is optional, but key was not passed, so we can skip it
            return;
        }
        $this->rules->each(function($rule){
            $method = sprintf('is%s', ucfirst($rule));
            if(!call_user_func([$this, $method])) {
                throw new ValidationException(
                    sprintf("Parameter '%s' failed the following validation: %s", $this->parameterName, $rule)
                );
            }
        });
    }

    protected function isRequired()
    {
        return $this->data !== null;
    }

    protected function isEmail()
    {
        return filter_var($this->data, FILTER_VALIDATE_EMAIL);
    }

    protected function isSometimes()
    {
        return true;
    }

    protected function isUrl()
    {
        return filter_var($this->data, FILTER_VALIDATE_URL);
    }

    protected function isDate()
    {
        $value = $this->data;

        if ($value instanceof DateTimeInterface) {
            return true;
        }
        if ((! is_string($value) && ! is_numeric($value)) || strtotime($value) === false) {
            return false;
        }
        $date = date_parse($value);

        return checkdate($date['month'], $date['day'], $date['year']);
    }
}
