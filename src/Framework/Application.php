<?php
namespace App\Framework;

use App\Framework\Bootstrap\BootstrapDatabase;
use App\Framework\Bootstrap\BootstrapMailer;
use App\Framework\Exception\Http401Unauthorized;
use App\Framework\Exception\Http403ForbiddenException;
use App\Framework\Exception\Http404NotFoundException;
use App\Framework\Exception\ValidationException;
use App\Framework\Http\JsonResponse;
use App\Framework\Http\Request;
use App\Framework\Http\Response;
use App\Framework\Router\ControllerResolver;
use AltoRouter;
use Dotenv\Dotenv;

class Application
{
    private $controllerResolver;
    private $container;

    public function __construct()
    {
        $this->container = new ServiceContainer();
        $this->controllerResolver = new ControllerResolver($this->createRouter());
        $this->bootstrap();
    }

    /**
     * @return \AltoRouter
     */
    protected function createRouter()
    {
        // TODO: load routes from file
        $router = new AltoRouter();
        $router->addRoutes([
            // access token
            ['POST', '/oauth/token', 'AccessTokenController#create'],
            // users api
            ['GET', '/api/users', 'Admin\UsersController#index'],
            ['POST', '/api/users', 'Admin\UsersController#create'],
            ['GET', '/api/users/[i:id]', 'Admin\UsersController#show'],
            ['PUT', '/api/users/[i:id]', 'Admin\UsersController#update'],
            // user's licenses api - attachs licenses to a user
            ['GET', '/api/users/[i:userId]/licenses', 'Admin\UserLicensesController#index'],
            ['POST', '/api/users/[i:userId]/licenses', 'Admin\UserLicensesController#create'],
            // location's api
            ['GET', '/api/locations', 'Admin\LocationsController#index'],
            ['POST', '/api/locations', 'Admin\LocationsController#create'],
            ['GET', '/api/locations/[i:id]', 'Admin\LocationsController#show'],
            ['PUT', '/api/locations/[i:id]', 'Admin\LocationsController#update'],
            // licenses api
            ['GET', '/api/licenses', 'Admin\LicensesController#index'],
            ['POST', '/api/licenses', 'Admin\LicensesController#create'],
            ['GET', '/api/licenses/[i:id]', 'Admin\LicensesController#show'],
            ['PUT', '/api/licenses/[i:id]', 'Admin\LicensesController#update'],
            // requests api
            ['GET', '/api/remote-working-requests', 'Admin\RemoteWorkingRequestsController#index'],
            ['PUT', '/api/remote-working-requests/[i:id]', 'Admin\RemoteWorkingRequestsController#update'],
            // user api
            ['GET', '/api/me/remote-working-requests', 'User\UserRemoteWorkingRequestsController#index'],
            ['POST', '/api/me/remote-working-requests', 'User\UserRemoteWorkingRequestsController#create'],
            ['PUT', '/api/me/remote-working-requests/[i:id]', 'User\UserRemoteWorkingRequestsController#update'],
        ]);
        $this->container->add('router', $router);
        return $router;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request)
    {
        try {
            // we make the container accessible to the request
            $request->setContainer($this->container);
            $response = $this->controllerResolver->createResponse($request);
        } catch (Http404NotFoundException $exception) {
            $response = new Response('', 404);
        } catch (Http401Unauthorized $exception) {
            $response = new JsonResponse(json_encode(['message' => $exception->getMessage()]), 401);
        } catch (Http403ForbiddenException $exception) {
            $response = new JsonResponse(json_encode(['message' => $exception->getMessage()]), 403);
        } catch (ValidationException $exception) {
            $response = new JsonResponse($exception->toJson(), 400);
        } catch (\Exception $exception) {
            $response = new Response($exception->getMessage(), 500);
        }

        return $response;
    }

    private function bootstrap()
    {
        try {
            // load .env variables
            $dotenv = Dotenv::create($this->baseDir());
            $dotenv->load();
        } catch (\Exception $exception) {}
        // boostrap all dependencies
        $this->container->add('database', BootstrapDatabase::setup());
        $this->container->add('mailer', BootstrapMailer::setup());
    }

    public function get($key)
    {
        return $this->container->get($key);
    }

    protected function baseDir($path = '')
    {
        $stack = debug_backtrace();
        $firstFrame = $stack[count($stack) - 1];
        return realpath(dirname($firstFrame['file']) . "/../" . $path);
    }
}
