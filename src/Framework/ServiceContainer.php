<?php

namespace App\Framework;


use Illuminate\Support\Collection;

class ServiceContainer
{
    protected $services;

    public function __construct()
    {
        $this->services = new Collection();
    }

    public function get($service)
    {
        return $this->services->get($service);
    }

    public function add($service, $implementation)
    {
        return $this->services->put($service, $implementation);
    }
}
