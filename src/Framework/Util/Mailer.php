<?php
namespace App\Framework\Util;

use App\Emails\Mail;
use Illuminate\Support\Collection;
use PHPMailer\PHPMailer\PHPMailer;

class Mailer
{
    protected $queue;

    public function __construct(array $config = [])
    {
        $this->config = collect(array_replace([
            'driver' => 'smtp',
            'host' => 'smtp.mailtrap.io',
            'port' => 2525,
            'username' => '',
            'password' => '',
            'from_address' => 'from@example.com',
            'from_name' => 'Example'
        ], $config));
        $this->queue = new Collection();
    }

    public function send(Mail $mail)
    {
        $this->addToQueue($mail);
        // if the driver is test, we just keep the mail in the queue
        // for future assertions
        if ($this->config->get('driver') === 'test') return;
        // we send the email using smtp
        $this->processQueue();
    }

    protected function processQueue()
    {
        $this->queue->each(function (Mail $mail) {
            $mailer = new PHPMailer(true);
            $mailer->isSMTP();
            $mailer->SMTPAuth   = true;
            $mailer->Host = $this->config->get('host');
            $mailer->Port = $this->config->get('port');
            $mailer->Username = $this->config->get('username');
            $mailer->Password = $this->config->get('password');
            $mailer->From = $this->config->get('from_address');
            $mailer->FromName = $this->config->get('from_name');

            // set the mail content
            $mailer->Subject = $mail->subject();
            $mailer->addAddress($mail->recipient());
            $mailer->isHTML(true);
            $mailer->Body = $mail->body();

            $mailer->send();
        });
    }

    protected function addToQueue(Mail $mail)
    {
        $this->queue->add($mail);

        return $this;
    }

    public function queueSize() : int
    {
        return $this->queue->count();
    }

    public function hasQueuedEmailOfType($class) : bool
    {
        return $this->queue->contains(function ($value) use ($class) {
            return get_class($value) === $class;
        });
    }
}
