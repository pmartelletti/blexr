<?php
namespace App\Framework\Bootstrap;


interface BootstrapsDependency
{
    public static function setup();
}
