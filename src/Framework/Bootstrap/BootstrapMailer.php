<?php

namespace App\Framework\Bootstrap;

use App\Framework\Util\Mailer;

class BootstrapMailer implements BootstrapsDependency
{
    public static function setup() : Mailer
    {
        // TODO: Implement setup() method.
        $mailer = new Mailer([
            'driver' => env('MAIL_DRIVER', 'smtp'),
            'host' => env('MAIL_HOST', 'smtp.mailtrap.io'),
            'port' => env('MAIL_PORT', 2525),
            'username' => env('MAIL_USERNAME', ''),
            'password' => env('MAIL_USERNAME',''),
            'from_address' => env('MAIL_FROM_ADDRESS',''),
            'from_name' => env('MAIL_FROM_NAME','')
        ]);

        return $mailer;
    }

}