<?php
namespace App\Controllers;

use App\Framework\Exception\Http401Unauthorized;
use App\Framework\Exception\Http403ForbiddenException;
use App\Framework\Http\Request;

abstract class ApiController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        if (!$request->user()) {
            throw new Http401Unauthorized;
        }
    }

    public function checkAdminMiddleware()
    {
        if (!$this->user()->isAdmin()) {
            throw new Http403ForbiddenException;
        }
    }
}
