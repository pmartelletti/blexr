<?php

namespace App\Controllers;

use App\Framework\Exception\ValidationException;
use App\Models\AccessToken;
use App\Models\User;

/**
 *  Simplified version of OAuthServer - not the proper implementation but works for
 *  the tasks purposes.
 */
class AccessTokenController extends Controller
{
    public function create()
    {
        /** @var User | null $user */
        $user = User::where([
            'email' => $this->request->get('username'),
            'password' => $this->request->get('password')
        ])->first();

        if ($user === null) {
            throw new ValidationException('Invalid user credentials');
        }
        $token = bin2hex(random_bytes(40));
        $user->accessTokens()->create([
            'id' => $token,
            'revoked' => false,
        ]);

        return $this->json(json_encode([
            'access_token' => $token
        ]));
    }
//->whereUserId($user->getKey())
//->where('revoked', 0)
//->where('expires_at', '>', Carbon::now())
//->latest('expires_at')
//->first();
}