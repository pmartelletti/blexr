<?php

namespace App\Controllers\User;

use App\Controllers\ApiController;
use App\Models\RemoteWorkingRequest;

class UserRemoteWorkingRequestsController extends ApiController
{
    public function index()
    {
        $requests = $this->user()->remoteWorkingRequests()->get()->toJson();
        return $this->json($requests);
    }

    public function create()
    {
        $this->validate([
            'requested_date' => 'required|date',
//            'location_id' => 'sometimes|required|exists:App\Location',
            'location_id' => 'sometimes|required',
            'employee_is_sick' => 'required',
        ]);
        // validate the request; throw exception if there's something wrong
        RemoteWorkingRequest::validate($this->request->getContent());
        // send new work request
        $request = $this->user()->remoteWorkingRequests()->create($this->request->getContentAsArray());

        return $this->json($request->toJson());
    }

    public function show()
    {
        // show a work request
    }

    public function update($id)
    {
        // TODO: do we need some validation?
        // update a work request
        $request = RemoteWorkingRequest::findOrFail($id);
        $request->update($this->request->getContentAsArray());

        return $this->json($request->toJson());
    }
}
