<?php

namespace App\Controllers;


use App\Framework\Http\JsonResponse;
use App\Framework\Http\Request;
use App\Framework\Security\Validator;
use App\Framework\Util\Mailer;
use App\Models\User;

abstract class Controller
{
    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function json($content, $status = 200, $headers = [])
    {
        return new JsonResponse($content, $status, $headers);
    }

    protected function validate(array $rules)
    {
        // validate required fields for now
        collect($rules)->each(function($rule, $key){
            (new Validator($key, $this->request->get($key), $rule))->validate();
        });

    }

    public function user()
    {
        return $this->request->user();
    }

    public function getService($service)
    {
        return $this->request->getContainer()->get($service);
    }
}
