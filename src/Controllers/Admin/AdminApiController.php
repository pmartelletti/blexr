<?php

namespace App\Controllers\Admin;

use App\Controllers\ApiController;
use App\Framework\Http\Request;

abstract class AdminApiController extends ApiController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->checkAdminMiddleware();
    }
}