<?php

namespace App\Controllers\Admin;

use App\Models\Location;

class LocationsController extends AdminApiController
{
    public function index()
    {
        $users = Location::all()->toJson();

        return $this->json($users);
    }


    public function create()
    {
        $this->validate([
            'name' => 'required'
        ]);
        $location = Location::create($this->request->getContentAsArray());

        return $this->json($location->toJson(), 201);
    }

    public function show($id)
    {
        $location = Location::find($id);

        return $this->json($location->toJson());
    }

    public function update($id)
    {
        $location = Location::findOrfail($id);
        $this->validate([
            'name' => 'required'
        ]);
        $location->update($this->request->getContentAsArray());

        return $this->json($location->toJson(), 200);
    }
}
