<?php

namespace App\Controllers\Admin;

use App\Emails\RemoteWorkRequestStatusChanged;
use App\Models\RemoteWorkingRequest;

class RemoteWorkingRequestsController extends AdminApiController
{
    public function index()
    {
        $requests = RemoteWorkingRequest::filter($this->request)->get()->toJson();
        return $this->json($requests);
    }

    public function update($id)
    {
        // we don't validate if the status is one of the possible statuses, as that will be done
        // at database level, as the field is enum
        $this->validate([
            'status' => 'required'
        ]);
        $request = RemoteWorkingRequest::findOrFail($id);
        $request->changeStatus($this->request->get('status'));
        // send email
        $this->getService('mailer')->send(new RemoteWorkRequestStatusChanged($this->user(), $request));

        return $this->json($request->toJson());
    }
}
