<?php

namespace App\Controllers\Admin;

use App\Models\License;
use App\Models\User;

class UserLicensesController extends AdminApiController
{
    public function index($userId)
    {
        $licenses = User::findOrFail($userId)->licenses;

        return $this->json($licenses);
    }

    public function create($userId)
    {
        $this->validate([
            'license_id' => 'required'
        ]);
        $license = License::findOrFail($this->request->get('license_id'));
        User::findOrFail($userId)->licenses()->attach($license);

        return $this->json('', 201);
    }
}
