<?php

namespace App\Controllers\Admin;

use App\Framework\Http\JsonResponse;
use App\Models\User;
use Illuminate\Database\Capsule\Manager as Capsule;

class UsersController extends AdminApiController
{
    public function index()
    {
        $users = User::all()->toJson();
        return $this->json($users);
    }

    public function create()
    {
        $this->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'job_title' => 'required'
        ]);

        $password = uniqid();
        $user = User::create($this->request->getContent()->merge(['password' => $password])->toArray());
        // send email!

        return new JsonResponse($user->toJson(), 201);
    }

    public function show($id)
    {
        $user = User::find($id);

        return new JsonResponse($user->toJson(), 200);
    }

    public function update($id)
    {
        $this->validate([
            'email' => 'sometimes|required|email',
        ]);
        $user = User::find($id);
        $user->update($this->request->getContentAsArray());

        return new JsonResponse($user->toJson(), 200);
    }
}
