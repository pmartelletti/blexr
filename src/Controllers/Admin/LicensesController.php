<?php

namespace App\Controllers\Admin;


use App\Models\License;

class LicensesController extends AdminApiController
{
    public function index()
    {
        $users = License::all()->toJson();

        return $this->json($users);
    }


    public function create()
    {
        $this->validate([
            'name' => 'required'
        ]);
        $license = License::create($this->request->getContentAsArray());

        return $this->json($license->toJson(), 201);
    }

    public function show($id)
    {
        $license = License::find($id);

        return $this->json($license->toJson());
    }

    public function update($id)
    {
        $license = License::findOrfail($id);
        $this->validate([
            'name' => 'required'
        ]);
        $license->update($this->request->getContentAsArray());

        return $this->json($license->toJson(), 200);
    }
}
