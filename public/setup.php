<?php
require_once __DIR__.'/../vendor/autoload.php';

use App\Database\AccessTokenSchema;
use App\Database\LicenseSchema;
use App\Database\LocationSchema;
use App\Database\RemoteWorkingRequestSchema;
use App\Database\UserLicenseSchema;
use App\Database\UserSchema;
use App\Framework\Application;


$application = new Application();
// run this script setup initial schema
(new UserSchema($application))->create();
(new LocationSchema($application))->create();
(new RemoteWorkingRequestSchema($application))->create();
(new LicenseSchema($application))->create();
(new UserLicenseSchema($application))->create();
(new AccessTokenSchema($application))->create();

