<?php
require_once __DIR__.'/../vendor/autoload.php';

use App\Framework\Application;

$app = new Application();

\App\Models\User::create([
    'first_name' => 'Pablo',
    'last_name' => 'Martelletti',
    'password' => '123456',
    'email' => 'mail@blexr.com',
    'job_title' => 'Office Administrator',
    'is_admin' => true
]);
//
\App\Models\License::create([
    'name' => 'Email Access Granted'
]);

\App\Models\License::create([
    'name' => 'Github Access Granted'
]);

\App\Models\Location::create([
    'name' => "Employee's Home"
]);

\App\Models\Location::create([
    'name' => "Malta Office - Sliema"
]);

\App\Models\Location::create([
    'name' => "Barcelona Office"
]);
