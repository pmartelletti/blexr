<?php
require_once __DIR__.'/../vendor/autoload.php';

use App\Framework\Application;
use App\Framework\Http\Request;

$app = new Application();
$response = $app->handle(Request::createFromGlobals());
$response->send();
